# Google

## Get static map url
```
use Lkt\Plugins\Google\StaticMap;
$map = StaticMap::getInstance('YOUR_API_KEY')
            ->setZoom(15)
            ->getUrl();
```

## Reverse Geocode
Needs: https://console.cloud.google.com/apis/library/geocoding-backend.googleapis.com

```
use Lkt\Helper\Location;

$geocode = \Lkt\Plugins\Google\Geocode::getInstance('YOUR_API_KEY',
            Location::getInstance()
            ->setLatitude($lat)
            ->setLongitude($lng)
        )->reverseGeocode();
```


## ODBC Config for SQL Server

nano /etc/odbc.ini

[MSSQLServer]
Driver = FreeTDS
Description = Any description
Trace = No
Server = 10.0.0.1
Port = 1433
TDS version = 0.95
Database =


nano /etc/odbcinst.ini

[FreeTDS]
Description = Freetds v 0.95
Driver = /lib64/libtdsodbc.so.0


Installation:
- [Ubuntu](https://stackoverflow.com/questions/12031897/missing-libtdsodbc-so-in-freetds-dev-mssql-on-ubuntu)

