<?php


namespace Lkt\Config;


use Lkt\Core\Context;
use Lkt\Drivers\Curl;
use Lkt\Drivers\MySql;
use Lkt\Traits\SingleTon;
use Symfony\Component\Console\Command\Command;

class Settings
{
    use SingleTon;

    protected $documentRoot = '';
    protected $pathsToModelsConfig = [];
    protected $pathsToRoutesConfig = [];
    protected $pathsToMigrations = [];
    protected $pathsToSeeds = [];
    protected $connections = [];
    protected $consoleCommands = [];

    const CONNECTION_TYPE_CURL = 0;
    const CONNECTION_TYPE_MYSQL = 1;

    /**
     * @param string $path
     * @return $this
     */
    public function addPathToModelsConfig(string $path = '') :Settings
    {
        $this->pathsToModelsConfig[] = $path;
        return $this;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function addPathToRoutesConfig(string $path = '') :Settings
    {
        $this->pathsToRoutesConfig[] = $path;
        return $this;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function addPathToMigrations(string $path = '') :Settings
    {
        $this->pathsToMigrations[] = $path;
        return $this;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function addPathToSeeds(string $path = '') :Settings
    {
        $this->pathsToSeeds[] = $path;
        return $this;
    }

    public function getPathsToMigrations()
    {
        return $this->pathsToMigrations;
    }

    public function getPathsToSeeds()
    {
        return $this->pathsToSeeds;
    }

    /**
     * @param string $name
     * @param array $connectionConfig
     * @return $this
     */
    public function addConnection(string $name = '', int $type = self::CONNECTION_TYPE_MYSQL, array $connectionConfig = []) :Settings
    {
        if (!isset($this->connections[$type]) || !\is_array($this->connections[$type])){
            $this->connections[$type] = [];
        }
        $this->connections[$type][$name] = $connectionConfig;
        return $this;
    }

    /**
     * @param \Symfony\Component\Console\Command\Command $command
     * @return $this
     */
    public function addConsoleCommand(Command $command) :Settings
    {
        $this->consoleCommands[] = $command;
        return $this;
    }

    /**
     * @return array
     */
    public function getConsoleCommands() :array
    {
        return $this->consoleCommands;
    }

    /**
     * @param string $documentRoot
     * @return \Lkt\Config\Settings
     */
    public function setDocumentRoot(string $documentRoot = '') :Settings
    {
        $this->documentRoot = \trim($documentRoot);
        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentRoot() :string
    {
        return $this->documentRoot;
    }

    /**
     * @return $this
     */
    public function loadModelsConfig() :Settings
    {
        foreach ($this->pathsToModelsConfig as $path) {
            $files = \scandir($path);
            foreach ($files as $file){
                if (\is_file("{$path}/{$file}")) {
                    require_once "{$path}/{$file}";
                }
            }
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function loadRoutesConfig() :Settings
    {
        foreach ($this->pathsToRoutesConfig as $path) {
            $files = \scandir($path);
            foreach ($files as $file){
                if (\is_file("{$path}/{$file}")) {
                    require_once "{$path}/{$file}";
                }
            }
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function loadConnectionsConfig() :Settings
    {
        // MySQL connections
        if (isset($this->connections[self::CONNECTION_TYPE_MYSQL])){
            foreach ($this->connections[self::CONNECTION_TYPE_MYSQL] as $name => $config){
                Context::setConnection($name, new MySql($config));
            }
        }

        // cURL connections
        if (isset($this->connections[self::CONNECTION_TYPE_CURL])) {
            foreach ($this->connections[self::CONNECTION_TYPE_CURL] as $name => $config) {
                Context::setConnection($name, new Curl($config));
            }
        }

        return $this;
    }

    public function load()
    {
        return $this
            ->loadModelsConfig()
            ->loadRoutesConfig()
            ->loadConnectionsConfig()
            ;
    }
}