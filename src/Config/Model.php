<?php


namespace Lkt\Config;

use Lkt\Core\Context;
use Lkt\Fields\Config\AbstractField;
use Lkt\Fields\Config\PivotField;
use Lkt\Fields\Config\ReverseForeignKeyField;
use Lkt\Traits\Instantiable;

/**
 * Class Model
 *
 * @package Lkt\Config
 */
class Model
{
    use Instantiable;

    protected $identifier = '';
    protected $table = '';
    protected $className = '';
    protected $generatedPath = '';
    protected $generatedNamespace = '';
    protected $generatedClassName = '';

    /** @var \Lkt\Fields\Config\AbstractField[] */
    protected $fields = [];

    /**
     * @param \Lkt\Fields\Config\AbstractField[] $fields
     * @return $this
     */
    public function setFields($fields) :self
    {
        $this->fields = [];
        foreach ($fields as $field){
            $this->addField($field);
        }
        return $this;
    }

    /**
     * @param \Lkt\Fields\Config\AbstractField $field
     *
     * @return $this
     */
    public function addField(AbstractField $field) :self
    {
        $this->fields[$field->getApplicationName()] = $field;
        return $this;
    }

    /**
     * @param string $identifier
     * @return $this
     */
    public function setIdentifier(string $identifier = '') :self
    {
        $this->identifier = \trim($identifier);
        return $this;
    }

    public function setGeneratedPath(string $path = '')
    {
        $this->generatedPath = $path;
        return $this;
    }

    public function setGeneratedClassName(string $name = '')
    {
        $this->generatedClassName = $name;
        return $this;
    }

    public function setGeneratedNamespace(string $namespace = '')
    {
        $this->generatedNamespace = $namespace;
        return $this;
    }

    public function getGeneratedPath()
    {
        return $this->generatedPath;
    }

    public function getGeneratedClassName()
    {
        return $this->generatedClassName;
    }

    public function getGeneratedNamespace()
    {
        return $this->generatedNamespace;
    }

    /**
     * @param string $table
     * @return $this
     */
    public function setTable(string $table = '') :self
    {
        $this->table = \trim($table);
        return $this;
    }

    /**
     * @param string $className
     *
     * @return \Lkt\Config\Model
     */
    public function setClassName(string $className = '') :self
    {
        $this->className = $className;
        return $this;
    }

    public function save()
    {
        Context::setModel($this->identifier, $this);
    }

    /**
     * @return \Lkt\Fields\Config\AbstractField[]
     */
    public function getFields() :array
    {
        return $this->fields;
    }

    /**
     * @return \Lkt\Fields\Config\AbstractField
     */
    public function getField($fieldName = '') :AbstractField
    {
        return $this->fields[$fieldName];
    }

    /**
     * @return string
     */
    public function getQueryFields() :string
    {
        $r = [];
        foreach ($this->fields as $field){
            if (!$field instanceof PivotField && !$field instanceof ReverseForeignKeyField){
                $r[] = $field->getQueryString();
            }
        }

        return \implode(', ', $r);
    }

    /**
     * @return string
     */
    public function getTable() :string
    {
        return $this->table;
    }

    /**
     * @return \Lkt\Fields\Config\AbstractField
     */
    public function getPrimaryKeyField() :AbstractField
    {
        foreach ($this->getFields() as $field){
            if ($field->isPrimaryKey()){
                return $field;
            }
        }

        return $this->fields[0];
    }

    /**
     * @return string
     */
    public function getClassName() :string
    {
        return \trim($this->className);
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

}