<?php

namespace Lkt\Interfaces;

/**
 * Interface FieldValidatorInterface
 * @package Lkt\Interfaces
 */
interface FieldValidatorInterface
{
    /**
     * @param bool $forceFresh
     *
     * @return \Lkt\Interfaces\FieldValidatorInterface
     */
    public static function getInstance($forceFresh = false);

    /**
     * @return bool
     */
    public function handle() :bool;
}