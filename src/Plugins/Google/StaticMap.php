<?php

namespace Lkt\Plugins\Google;

use Lkt\Drivers\Curl;
use Lkt\Traits\Instantiable;

/**
 * Class StaticMap
 *
 * @package Lkt\Plugins\Google
 */
class StaticMap
{
    use Instantiable;

    protected $zoom = '';
    protected $latitude = '';
    protected $longitude = '';
    protected $width = 350;
    protected $height = 150;
    protected $markers = [];
    protected $mapType = 'roadmap';
    protected $output = 'embed';
    protected $apiKey = '';

    public function __construct($apiKey = '')
    {
        $this->apiKey = \trim($apiKey);
    }

    /**
     * @param int $zoom
     * @return $this
     */
    public function setZoom($zoom = 0)
    {
        $this->zoom = (int)$zoom;
        return $this;
    }

    /**
     * @param string $latitude
     * @return $this
     */
    public function setLatitude($latitude = ''){
        $this->latitude = \trim($latitude);
        return $this;
    }

    /**
     * @param string $longitude
     * @return $this
     */
    public function setLongitude($longitude = ''){
        $this->longitude = \trim($longitude);
        return $this;
    }

    public function setWidth($width = 0)
    {
        $this->width = (float)$width;
        return $this;
    }

    public function setHeigth($height = 0)
    {
        $this->height = (float)$height;
        return $this;
    }

    public function setMarkers($markers = [])
    {
        $this->markers = $markers;
        return $this;
    }

    public function setMapType($mapType = '')
    {
        $this->mapType = \trim($mapType);
        return $this;
    }
    
    public function toArray()
    {
        $r = [];

        if ($this->zoom !== ''){
            $r['zoom'] = $this->zoom;
        }

        if ($this->latitude !== '' && $this->longitude){
            $r['center'] = "{$this->latitude},{$this->longitude}";
        }

        if ($this->width !== '' && $this->height !== ''){
            $r['size'] = "{$this->width}x{$this->height}";
        }

        if ($this->mapType !== ''){
            $r['maptype'] = $this->mapType;
        }

        $r['output'] = $this->output;

        return $r;
    }

    public function getUrl()
    {
        $args = $this->toArray();
        $args['key'] = $this->apiKey;

        $curl = Curl::getInstance([
            'Host' => 'https://maps.googleapis.com'
        ]);

        return \trim($curl->buildUri('/maps/api/staticmap', $args));
    }
}