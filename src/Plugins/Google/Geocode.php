<?php

namespace Lkt\Plugins\Google;

use Lkt\Drivers\Curl;
use Lkt\Helper\Location;
use Lkt\Helper\Locations;
use Lkt\Traits\Instantiable;

/**
 * Class Geocode
 *
 * @package Lkt\Plugins\Google
 */
class Geocode
{
    use Instantiable;

    /**
     * @var \Lkt\Helper\Location
     */
    protected $location;

    /**
     * @var string
     */
    protected $apiKey = '';

    /**
     * Geocode constructor.
     *
     * @param string $apiKey
     * @param \Lkt\Helper\Location $location
     */
    public function __construct($apiKey = '', Location $location)
    {
        $this->apiKey = \trim($apiKey);
        $this->location = $location;
    }

    /**
     * @return \Lkt\Helper\Locations
     */
    public function geocode()
    {
        $curl = Curl::getInstance([
            'Host' => 'https://maps.googleapis.com'
        ]);

        return $this->parseResults(\json_decode($curl->query('/maps/api/geocode/json', [
            'address' => $this->location->toGeocode(),
            'key' => $this->apiKey
        ])));
    }

    /**
     * @return \Lkt\Helper\Locations
     */
    public function reverseGeocode()
    {
        $curl = Curl::getInstance([
            'Host' => 'https://maps.googleapis.com'
        ]);

        return $this->parseResults(\json_decode($curl->query('/maps/api/geocode/json', [
            'latlng' => $this->location->toReverseGeocode(),
            'key' => $this->apiKey
        ])));
    }

    /**
     * @param $r
     * @return \Lkt\Helper\Locations
     */
    protected function parseResults($r)
    {
        $locations = Locations::getInstance();
        $stack = [];

        foreach ($r->results as $result){
            $location = Location::getInstance();

            foreach ($result->address_components as $component){
                switch ($component->types[0]){
                    case 'postal_code':
                        $location->setPostalCode($component->long_name);
                        break;
                    case 'locality':
                        $location->setCity($component->long_name);
                        break;
                    case 'administrative_area_level_2':
                        $location->setProvince($component->long_name);
                        break;
                    case 'country':
                        $location->setCountry($component->long_name);
                        break;

                }
            }

            $location->setLatitude($result->geometry->location->lat);
            $location->setLongitude($result->geometry->location->lng);
            $stackKey = $location->toStack();
            if (!\in_array($stackKey, $stack)){
                $locations[] = $location;
                $stack[] = $stackKey;
            }
        }

        return $locations;
    }
}
