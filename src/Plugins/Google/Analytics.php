<?php

namespace Lkt\Plugins\Google;

use Lkt\Traits\Instantiable;

/**
 * Class Analytics
 *
 * @package Lkt\Plugins\Google
 */
class Analytics
{
    use Instantiable;

    protected $userAccount = '';

    public function __construct($userAccount = '')
    {
        $this->userAccount = \trim($userAccount);
    }

    /**
     * @return string
     */
    public function renderScript()
    {
        if ($this->userAccount !== '') {
            return "<script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');</script>";
        }
        return '';
    }

    /**
     * @param int $httpStatus
     * @return string
     */
    public function trackPage($httpStatus = 200)
    {
        $html = '';
        if ($this->userAccount !== '') {
            $html .= "
                ga('create','{$this->userAccount}','auto');
                ga('require', 'displayfeatures');
                ga('send','pageview');
            ";

            if ($httpStatus === 404) {
                $html .= "
                ga('send', 'event', 'Error 404', document.location.pathname + document.location.search, document.referrer, {'nonInteraction': 1});
            ";
            }

            return "<script>{$html}</script>";
        }
        return '';
    }

    public function trackEcommerce()
    {
        $html = "
            ga('create', '{$this->userAccount}', 'auto');
            ga('send', 'pageview');
            ga('require', 'ecommerce');
        ";

        return $html;
    }

    public function trackTransaction($orderId = 0, $storeName = '', $price = 0.00, $tax = 0.00, $shippingCost = 0.00, $customerCity = '', $customerState = '', $customerCountry = '')
    {
        $html = "
            ga('ecommerce:addTransaction', {
              'id': '{$orderId}',
              'affiliation': '{$storeName}',
              'revenue': '{$price}',
              'shipping': '{$shippingCost}',
              'tax': '{$tax}'
            });
        ";

        return $html;
    }

    public static function trackTransactionItem($id = 0, $name = '', $sku = '', $category = '', $price = '', $quantity = '')
    {
        $html = "
            ga('ecommerce:addItem', {
              'id': '{$id}',
              'name': '{$name}',
              'sku': '{$sku}',
              'category': '',
              'price': '{$price}',
              'quantity': '{$quantity}'
            });
        ";

        return $html;
    }

    public static function renderTransactionSend()
    {
        $html = "
            ga('ecommerce:send');
        ";

        return $html;
    }
}