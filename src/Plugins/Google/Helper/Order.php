<?php

namespace Lkt\Plugins\Google\Helper;

use Lkt\Traits\Instantiable;
use Lkt\Traits\MagicalGet;

/**
 * Class Order
 *
 * @package Lkt\Plugins\Google\Helper
 */
class Order
{
    use Instantiable;
    use MagicalGet;
    
    public $id = '';
    public $store = '';

    public $price = 0.00;
    public $tax = 0.00;
    public $shippingCost = 0.00;

    protected $lines = [];

    /**
     * @param \Lkt\Plugins\Google\Helper\OrderLine $line
     * @return $this
     */
    public function addLine(OrderLine $line)
    {
        $this->lines[] = $line;
        return $this;
    }
}