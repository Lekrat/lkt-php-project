<?php

namespace Lkt\Plugins\Google\Helper;

use Lkt\Traits\Instantiable;
use Lkt\Traits\MagicalGet;

/**
 * Class AnalyticsPage
 *
 * @package Lkt\Plugins\Google\Helper
 */
class AnalyticsPage
{
    use Instantiable;
    use MagicalGet;
    
    protected $httpStatus = 200;
    protected $order;

    /**
     * AnalyticsPage constructor.
     *
     * @param int $httpStatus
     */
    public function __construct($httpStatus = 200)
    {
        $this->httpStatus = $httpStatus;
    }

    /**
     * @param \Lkt\Plugins\Google\Helper\Order $order
     * @return $this
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
        return $this;
    }
}