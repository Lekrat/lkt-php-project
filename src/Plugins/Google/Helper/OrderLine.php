<?php

namespace Lkt\Plugins\Google\Helper;


use Lkt\Traits\Instantiable;
use Lkt\Traits\MagicalGet;

class OrderLine
{
    use Instantiable;
    use MagicalGet;

    public $id = '';
    public $name = '';
    public $sku = '';
    public $category = '';
    public $price = '';
    public $quantity = '';
}