<?php


namespace Lkt\Console\Commands;

use Lkt\Config\Settings;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Migrate
 *
 * @package Lkt\Console\Commands
 */
class MigrateCommand extends Command
{
    protected static $defaultName = 'migrate';

    public function execute(InputInterface $input, OutputInterface $output)
    {
        require_once Settings::getInstance()->getDocumentRoot() . '/vendor/bin/phinx';
    }
}