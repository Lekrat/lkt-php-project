<?php

namespace Lkt\Console\Commands;

use Lkt\CodeFactory\ModelEngine;
use Lkt\Config\Settings;
use Lkt\Core\Context;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class MakeModels
 *
 * @package Lkt\Console\Commands
 */
class MakeModelsCommand extends Command
{
    protected static $defaultName = 'make:models';

    public function execute(InputInterface $input, OutputInterface $output)
    {
        Settings::getInstance()->loadModelsConfig();
        $models = Context::getModels();
        $l = \count($models);

        $output->writeln("Detected {$l} model(s)...");
        $output->writeln("===");
        $output->writeln("");
        ModelEngine::getInstance()->generate();
    }
}