<?php


namespace Lkt\CodeFactory;


use Lkt\Config\Settings;
use Lkt\Core\Context;
use Lkt\Traits\SingleTon;

class PhinxEngine
{
    use SingleTon;

    public function make()
    {
        $environments = [];

        $environments['default_database'] = 'default';
        $environments['default_migration_table'] = 'migrations';

        $i = 0;
        foreach (Context::getConnections() as $name => $connection){
            if ($i === 0){
                $environments['default_database'] = $name;
            }
            $info = $connection->toPhinx();
            if (\count($info) > 0){
                $environments[$name] = $connection->toPhinx();
                ++$i;
            }
        }

        $migrations = ['%%PHINX_CONFIG_DIR%%/db/migrations'];
        $seeds = ['%%PHINX_CONFIG_DIR%%/db/seeds'];

        foreach (Settings::getInstance()->getPathsToMigrations() as $migration){
            $migrations[] = $migration;
        }

        foreach (Settings::getInstance()->getPathsToSeeds() as $seed){
            $seeds[] = $seed;
        }

        $config = [
            'paths' => [
                'migrations' => $migrations,
                'seeds' => $seeds,
            ],
            'environments' => $environments
        ];

        \file_put_contents(Settings::getInstance()->getDocumentRoot() . '/phinx.json', \json_encode($config));
    }
}