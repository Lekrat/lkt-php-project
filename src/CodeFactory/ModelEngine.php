<?php


namespace Lkt\CodeFactory;

use Lkt\CodeFactory\Helpers\ModelEngineFieldHelper;
use Lkt\CodeFactory\Templates\CleanModelTemplate;
use Lkt\Core\Context;
use Lkt\Factory\ModelFactory;
use Lkt\Fields\Config\BooleanField;
use Lkt\Fields\Config\PivotField;
use Lkt\Fields\Config\ReverseForeignKeyField;
use Lkt\Traits\Instantiable;

/**
 * Class ModelEngine
 *
 * @package Lkt\CodeFactory
 */
class ModelEngine
{
    use Instantiable;

    public function generate()
    {
        $declarations = [];
        $getters = [];
        $setters = [];
        foreach (Context::getModels() as $model){
            foreach ($model->getFields() as $field){
                $getters[] = ModelEngineFieldHelper::getGetterDeclaration($field->getApplicationName(), $field->getDocumentationType());
                $setters[] = ModelEngineFieldHelper::getSetterDeclaration($field->getApplicationName(), $field->getDocumentationType());

                if ($field instanceof PivotField || $field instanceof ReverseForeignKeyField){
                    $getters[] = ModelEngineFieldHelper::getRelationalDeclarations($field->getApplicationName(), $field->getDocumentationType());
                }

                $namespace = $model->getGeneratedNamespace();
                if ($namespace === ''){
                    $namespace = '\\Generated\\Models';
                }

                $className = $model->getGeneratedClassName();

                $content = CleanModelTemplate::getInstance()
                    ->set('namespace', $namespace)
                    ->set('className', $className)
                    ->set('variables', \implode("\n", $declarations))
                    ->set('getters', \implode("\n", $getters))
                    ->set('setters', \implode("\n", $setters))
                    ->render();

                $content = "<?php
                    {$content}
                ";

                if (!\is_dir($model->getGeneratedPath())) {
                    \mkdir($model->getGeneratedPath(), 0777, true);
                }
                \file_put_contents($model->getGeneratedPath() . \DIRECTORY_SEPARATOR . $className . '.php', $content);
            }
        }
    }
}