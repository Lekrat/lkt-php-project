<?php

namespace Lkt\CodeFactory\Templates;

use Lkt\Core\Template;

/**
 * Class CleanModelTemplate
 * @package Lkt\CodeFactory\Templates
 */
class CleanModelTemplate extends Template
{
    /**
     * CleanModelTemplate constructor.
     *
     * @param string $path
     * @param string $file
     * @param string $extension
     */
    public function __construct(string $path = '', string $file = '', string $extension = 'tpl')
    {
        parent::__construct(__DIR__ . '/../../../resources/templates', 'model-engine-clean-class', 'tpl');
    }
}