<?php

namespace Lkt\CodeFactory\Helpers;

/**
 * Class ModelEngineFieldHelper
 * @package Lkt\CodeFactory\Helpers
 */
class ModelEngineFieldHelper
{
    /**
     * @param string $variableName
     * @param string $variableType
     * @param string $defaultValue
     * @param string $accessModifier
     * @return string
     */
    public static function getDeclaration(string $variableName = '', string $variableType = '', string $defaultValue = '', string $accessModifier = 'protected') :string
    {
        return "
    /** @var {$variableType} \${$variableName} */
    {$accessModifier} \${$variableName} = {$defaultValue};";
    }

    /**
     * @param string $variableName
     * @param string $variableType
     * @param string $defaultValue
     * @param string $accessModifier
     * @return string
     */
    public static function getGetterDeclaration(string $variableName = '', string $variableType = '', string $accessModifier = 'public') :string
    {
        $methodName = 'get'.\ucfirst($variableName);
        return "
    /** @return {$variableType}*/
    {$accessModifier} function {$methodName}()
    {
        return \$this->fields->{$variableName}->getValue(\$this);
    }";
    }

    /**
     * @param string $variableName
     * @param string $variableType
     * @param string $defaultValue
     * @param string $accessModifier
     * @return string
     */
    public static function getSetterDeclaration(string $variableName = '', string $variableType = '', string $accessModifier = 'public') :string
    {
        $methodName = 'set'.\ucfirst($variableName);
        return "
    /** @return \$this*/
    {$accessModifier} function {$methodName}(\$value)
    {
        \$this->fields->{$variableName}->setValue(\$this, \$value);
        return \$this;
    }";
    }

    /**
     * @param string $variableName
     * @param string $variableType
     * @param string $defaultValue
     * @param string $accessModifier
     * @return string
     */
    public static function getRelationalDeclarations(string $variableName = '', string $variableType = '', string $accessModifier = 'public') :string
    {
        $methodName = 'get'.\ucfirst($variableName);
        return "
    /** @return int*/
    {$accessModifier} function {$methodName}Total()
    {
        \$this->fields->{$variableName}->getTotal(\$this);
        return \$this;
    }";
    }
}