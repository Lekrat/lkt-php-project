<?php


namespace Lkt\Fields\Traits;

/**
 * Trait FloatFieldTrait
 * @package Lkt\Fields\Traits
 */
trait FloatFieldTrait
{
    protected $defaultValue = 0.0;

    /**
     * @return string
     */
    public function getDocumentationDefaultValue() :string
    {
        return \trim($this->defaultValue);
    }
}