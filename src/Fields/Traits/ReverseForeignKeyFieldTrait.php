<?php


namespace Lkt\Fields\Traits;

use Lkt\Traits\Config\WithModelIdentifierTrait;

/**
 * Trait ForeignKeyFieldTrait
 * @package Lkt\Fields\Traits
 */
trait ReverseForeignKeyFieldTrait
{
    use WithModelIdentifierTrait;

    protected $modelKey = '';
    protected $defaultValue = [];

    /**
     * @param string $key
     * @return $this
     */
    public function setModelKey($key = 'id') :self
    {
        $this->modelKey = $key;
        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentationDefaultValue() :string
    {
        return '[]';
    }
}