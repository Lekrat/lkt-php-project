<?php


namespace Lkt\Fields\Traits;

/**
 * Trait DateTimeFieldTrait
 * @package Lkt\Fields\Traits
 */
trait DateTimeFieldTrait
{
    protected $defaultValue = '00-00-00 00:00:00';
}