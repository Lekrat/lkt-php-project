<?php


namespace Lkt\Fields\Traits;

/**
 * Trait IntegerFieldTrait
 * @package Lkt\Fields\Traits
 */
trait IntegerFieldTrait
{
    protected $defaultValue = 0;

    /**
     * @return string
     */
    public function getDocumentationDefaultValue() :string
    {
        return \trim($this->defaultValue);
    }
}