<?php


namespace Lkt\Fields\Traits;

/**
 * Trait TextFieldTrait
 * @package Lkt\Fields\Traits
 */
trait TextFieldTrait
{
    protected $defaultValue = '';
}