<?php


namespace Lkt\Fields\Traits;

use Lkt\Traits\Config\WithModelIdentifierTrait;

/**
 * Trait PivotFieldTrait
 * @package Lkt\Fields\Traits
 */
trait PivotFieldTrait
{
    use WithModelIdentifierTrait;

    protected $modelKey = '';
    protected $constraints = [];
    protected $pivotTable = '';
    protected $pivotColumn1 = '';
    protected $pivotColumn2 = '';
    protected $defaultValue = [];

    /**
     * @param string $key
     * @return $this
     */
    public function setModelKey($key = 'id')
    {
        $this->modelKey = $key;
        return $this;
    }

    /**
     * @param string $table
     * @return $this
     */
    public function setPivotTable($table = '')
    {
        $this->pivotTable = $table;
        return $this;
    }

    /**
     * @param string $constraint
     * @return $this
     */
    public function addConstraint($constraint = '')
    {
        $this->constraints[] = $constraint;
        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentationType() :string
    {
        return 'pivot';
    }

    /**
     * @return string
     */
    public function getDocumentationDefaultValue() :string
    {
        return '[]';
    }

    /**
     * @return string
     */
    public function getPivotTable() :string
    {
        return $this->pivotTable;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setPivotColumn1(string $name = '') :self
    {
        $this->pivotColumn1 = $name;
        return $this;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setPivotColumn2(string $name = '') :self
    {
        $this->pivotColumn2 = $name;
        return $this;
    }
}