<?php


namespace Lkt\Fields\Traits;

/**
 * Trait BooleanFieldTrait
 * @package Lkt\Fields\Traits
 */
trait BooleanFieldTrait
{
    protected $defaultValue = false;

    public function getDocumentationDefaultValue(): string
    {
        return $this->defaultValue === true ? 'true' : 'false';
    }
}