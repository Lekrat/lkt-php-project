<?php


namespace Lkt\Fields\Traits;

/**
 * Trait HtmlFieldTrait
 * @package Lkt\Fields\Traits
 */
trait HtmlFieldTrait
{
    protected $defaultValue = '';
}