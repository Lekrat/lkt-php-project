<?php


namespace Lkt\Fields\Traits;

/**
 * Trait ForeignKeyFieldTrait
 * @package Lkt\Fields\Traits
 */
trait ForeignKeyFieldTrait
{
    protected $modelIdentifier = '';
    protected $modelKey = '';
    protected $defaultValue = 0;

    /**
     * @param $identifier
     * @return $this
     */
    public function setModelIdentifier($identifier)
    {
        $this->modelIdentifier = $identifier;
        return $this;
    }

    /**
     * @param string $key
     * @return $this
     */
    public function setModelKey($key = 'id')
    {
        $this->modelKey = $key;
        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentationDefaultValue() :string
    {
        return \trim($this->defaultValue);
    }
}