<?php


namespace Lkt\Fields\Traits;

/**
 * Trait StringFieldTrait
 * @package Lkt\Fields\Traits
 */
trait StringFieldTrait
{
    protected $defaultValue = '';
}