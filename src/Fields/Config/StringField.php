<?php


namespace Lkt\Fields\Config;

use Lkt\Fields\Traits\StringFieldTrait;

/**
 * Class StringField
 * @package Lkt\Fields\Config
 */
class StringField extends AbstractField
{
    use StringFieldTrait;

    public function getRuntimeField($item = null, $value = '')
    {
        return \Lkt\Fields\Runtime\StringField::getInstance()->setInitialValue($item, $value);
    }
}