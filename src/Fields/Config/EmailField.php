<?php


namespace Lkt\Fields\Config;

use Lkt\Fields\Traits\EmailFieldTrait;

/**
 * Class EmailField
 *
 * @package Lkt\Fields\Config
 */
class EmailField extends AbstractField
{
    use EmailFieldTrait;

    public function getRuntimeField($item = null, $value = '')
    {
        return \Lkt\Fields\Runtime\EmailField::getInstance()->setInitialValue($item, $value);
    }
}