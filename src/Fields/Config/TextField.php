<?php


namespace Lkt\Fields\Config;

use Lkt\Fields\Traits\TextFieldTrait;

/**
 * Class TextField
 * @package Lkt\Fields\Config
 */
class TextField extends AbstractField
{
    use TextFieldTrait;

    public function getRuntimeField($item = null, $value = '')
    {
        return \Lkt\Fields\Runtime\TextField::getInstance()->setInitialValue($item, $value);
    }
}