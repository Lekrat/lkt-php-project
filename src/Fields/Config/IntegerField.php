<?php


namespace Lkt\Fields\Config;

use Lkt\Fields\Traits\IntegerFieldTrait;

/**
 * Class IntegerField
 * @package Lkt\Fields\Config
 */
class IntegerField extends AbstractField
{
    use IntegerFieldTrait;
    const DOCUMENTATION_TYPE = 'int';

    public function getRuntimeField($item = null, $value = 0)
    {
        return \Lkt\Fields\Runtime\IntegerField::getInstance()->setInitialValue($item, $value);
    }
}