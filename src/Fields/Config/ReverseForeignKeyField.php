<?php


namespace Lkt\Fields\Config;

use Lkt\Fields\Traits\ReverseForeignKeyFieldTrait;

/**
 * Class ReverseForeignKeyField
 * @package Lkt\Fields\Config
 */
class ReverseForeignKeyField extends AbstractField
{
    use ReverseForeignKeyFieldTrait;
    const DOCUMENTATION_TYPE = 'int';

    public function getRuntimeField($item = null, $value = 0)
    {
        return \Lkt\Fields\Runtime\ReverseForeignKeyField::getInstance()
            ->setInitialValue($item, $value)
            ->setModelIdentifier($this->modelIdentifier)
            ->setModelKey($this->modelKey)
            ;
    }
}