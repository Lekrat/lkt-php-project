<?php


namespace Lkt\Fields\Config;

use Lkt\Fields\Traits\HtmlFieldTrait;

/**
 * Class HtmlField
 *
 * @package Lkt\Fields\Config
 */
class HtmlField extends AbstractField
{
    use HtmlFieldTrait;

    public function getRuntimeField($item = null, $value = '')
    {
        return \Lkt\Fields\Runtime\HtmlField::getInstance()->setInitialValue($item, $value);
    }
}