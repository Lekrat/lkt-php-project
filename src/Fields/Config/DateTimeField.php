<?php


namespace Lkt\Fields\Config;

use Lkt\Fields\Traits\DateTimeFieldTrait;

/**
 * Class DateTimeField
 * @package Lkt\Fields\Config
 */
class DateTimeField extends AbstractField
{
    use DateTimeFieldTrait;

    public function getRuntimeField($item = null, $value = '')
    {
        return \Lkt\Fields\Runtime\DateTimeField::getInstance()->setInitialValue($item, $value);
    }
}