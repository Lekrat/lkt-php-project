<?php


namespace Lkt\Fields\Config;

use Lkt\Fields\Traits\FloatFieldTrait;

/**
 * Class FloatField
 * @package Lkt\Fields\Config
 */
class FloatField extends AbstractField
{
    use FloatFieldTrait;
    const DOCUMENTATION_TYPE = 'float';

    public function getRuntimeField($item = null, $value = 0.0)
    {
        return \Lkt\Fields\Runtime\FloatField::getInstance()->setInitialValue($item, $value);
    }
}