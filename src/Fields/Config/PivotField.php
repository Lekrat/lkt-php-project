<?php


namespace Lkt\Fields\Config;

use Lkt\Fields\Traits\PivotFieldTrait;

/**
 * Class PivotField
 * @package Lkt\Fields\Config
 */
class PivotField extends AbstractField
{
    use PivotFieldTrait;

    const MODE_MULTIPLE_TO_MULTIPLE = 'mn';
    const MODE_MULTIPLE_TO_ONE = 'm1';

    public function getRuntimeField($item = null, $value = '')
    {
        return \Lkt\Fields\Runtime\PivotField::getInstance($this->mode)
            ->setInitialValue($item, $value)
            ->setModelIdentifier($this->modelIdentifier)
            ->setModelKey($this->modelKey)
            ->setPivotTable($this->getPivotTable())
            ->setPivotColumn1($this->pivotColumn1)
            ->setPivotColumn2($this->pivotColumn2)
            ;
    }
}