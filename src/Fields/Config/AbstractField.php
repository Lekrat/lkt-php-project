<?php

namespace Lkt\Fields\Config;

use Lkt\Interfaces\FieldValidatorInterface;
use Lkt\Traits\Instantiable;

/**
 * Class AbstractField
 * @package Lkt\Fields\Config
 */
abstract class AbstractField
{
    use Instantiable;

    protected $columnName = '';
    protected $applicationName = '';
    protected $isPrimaryKey = false;
    protected $validator;
    const DOCUMENTATION_TYPE = 'string';

    /**
     * @param string $name
     * @return $this
     */
    public function setColumnName(string $name = '') :self
    {
        $this->columnName = $name;
        return $this;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setApplicationName(string $name = '') :self
    {
        $this->applicationName = $name;
        return $this;
    }

    /**
     * @param $value
     *
     * @return \Lkt\Fields\Config\AbstractField
     */
    public function setDefaultValue($value) :self
    {
        $this->defaultValue = $value;
        return $this;
    }

    /**
     * @param bool $value
     *
     * @return \Lkt\Fields\Config\AbstractField
     */
    public function setPrimaryKey($value = false) :self
    {
        $this->isPrimaryKey = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    public function getColumnName() :string
    {
        return \trim($this->columnName);
    }

    public function getApplicationName() :string
    {
        $r = $this->applicationName;
        if ($r === ''){
            $r = \trim($this->getColumnName());
        }

        return $r;
    }

    /**
     * @return string
     */
    public function getQueryString() :string
    {
        $r = [$this->getColumnName(), $this->getApplicationName()];
        return '`' . \implode('` as `', $r) . '`';
    }

    /**
     * @return string
     */
    public function getValidationName()
    {
        return 'has'.\ucfirst($this->getApplicationName());
    }

    /**
     * @return string
     */
    public function getDocumentationType() :string
    {
        return static::DOCUMENTATION_TYPE;
    }

    /**
     * @return string
     */
    public function getDocumentationDefaultValue() :string
    {
        return \trim("'{$this->defaultValue}'");
    }

    /**
     * @return bool
     */
    public function isPrimaryKey() :bool
    {
        return $this->isPrimaryKey === true;
    }

    /**
     * @param \Lkt\Interfaces\FieldValidatorInterface $validator
     *
     * @return $this
     */
    public function setValidator(FieldValidatorInterface $validator = null)
    {
        $this->validator = $validator;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValidator()
    {
        return $this->validator;
    }
}