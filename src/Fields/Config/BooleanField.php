<?php


namespace Lkt\Fields\Config;

use Lkt\Fields\Traits\BooleanFieldTrait;

/**
 * Class FloatField
 *
 * @package Lkt\Fields
 */
class BooleanField extends AbstractField
{
    use BooleanFieldTrait;
    const DOCUMENTATION_TYPE = 'boolean';

    /**
     * @param bool $value
     *
     * @return \Lkt\Fields\Runtime\BooleanField
     */
    public function getRuntimeField($item = null, $value = false)
    {
        return \Lkt\Fields\Runtime\BooleanField::getInstance()->setInitialValue($item, $value === true);
    }
}