<?php


namespace Lkt\Fields\Config;

use Lkt\Fields\Traits\ForeignKeyFieldTrait;

/**
 * Class ForeignKeyField
 *
 * @package Lkt\Fields\Config
 */
class ForeignKeyField extends AbstractField
{
    use ForeignKeyFieldTrait;
    const DOCUMENTATION_TYPE = 'int';

    public function getRuntimeField($item = null, $value = 0)
    {
        return \Lkt\Fields\Runtime\ForeignKeyField::getInstance()->setInitialValue($item, $value);
    }
}