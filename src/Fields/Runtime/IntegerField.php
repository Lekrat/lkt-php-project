<?php


namespace Lkt\Fields\Runtime;

use Lkt\Fields\Traits\IntegerFieldTrait;

/**
 * Class IntegerField
 * @package Lkt\Fields\Runtime
 */
class IntegerField extends AbstractField
{
    use IntegerFieldTrait;

    protected $value = 0;
    protected $initialValue = 0;

    public function setValue($item = null, $value = 0)
    {
        $this->value = (int)$value;
        return $this;
    }

    public function setInitialValue($item = null, $value = 0)
    {
        $this->initialValue = (int)$value;
        return $this->setValue($item, $value);
    }

    /**
     * @return int
     */
    public function getValue() :int
    {
        return $this->value;
    }
}