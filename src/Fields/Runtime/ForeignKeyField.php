<?php


namespace Lkt\Fields\Runtime;

use Lkt\Fields\Traits\ForeignKeyFieldTrait;

/**
 * Class ForeignKeyField
 * @package Lkt\Fields\Runtime
 */
class ForeignKeyField extends AbstractField
{
    use ForeignKeyFieldTrait;

    protected $value = 0;
    protected $initialValue = 0;

    public function setValue($item = null, $value = 0)
    {
        $this->value = (int)$value;
        return $this;
    }

    public function setInitialValue($item = null, $value = 0)
    {
        $this->initialValue = (int)$value;
        return $this->setValue($item, $value);
    }
}