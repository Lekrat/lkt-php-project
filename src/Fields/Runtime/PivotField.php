<?php


namespace Lkt\Fields\Runtime;

use Lkt\Core\Context;
use Lkt\Factory\ModelFactory;
use Lkt\Fields\Traits\PivotFieldTrait;

/**
 * Class PivotField
 * @package Lkt\Fields\Runtime
 */
class PivotField extends AbstractField
{
    use PivotFieldTrait;

    const MODE_MULTIPLE_TO_MULTIPLE = 'mn';
    const MODE_MULTIPLE_TO_ONE = 'm1';

    protected $value = [];
    protected $initialValue = [];

    public function __construct($mode = '')
    {
        $this->mode = $mode;
    }

    public function setValue($item = null, $value = [])
    {
        $this->value = $value;
        return $this;
    }

    public function setInitialValue($item = null, $value = [])
    {
        $this->initialValue = $value;
        return $this->setValue($item, $value);
    }

    /**
     * @param $item
     *
     * @return array
     */
    public function getValue($item) :array
    {
        $fetch = ModelFactory::getInstance()
            ->setModel($this->modelIdentifier);

        $model = Context::getModel($this->modelIdentifier);

        if ($this->mode === static::MODE_MULTIPLE_TO_ONE){
            $fetch
                ->setFrom("{$model->getTable()}")
                ->setWhere("{$model->getTable()}.{$this->getColumnName()} = {$item->getModel()->getTable()}.{$this->modelKey}");

        } else {
            $fetch
                ->setFrom("{$model->getTable()} LEFT JOIN {$this->getPivotTable()} ON ({$this->getPivotTable()}.{$this->pivotColumn2}} = {$model->getTable()}.{$model->getPrimaryKeyField()->getColumnName()})")
                ->setWhere("{$this->getPivotTable()}.{$this->pivotColumn1} = {$model->getTable()}.{$item->getPrimaryKeyField()->getColumnName()}");
        }

        return $fetch->getResults();
    }
}