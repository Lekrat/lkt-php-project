<?php


namespace Lkt\Fields\Runtime;

use Lkt\Core\Context;
use Lkt\Factory\ModelFactory;
use Lkt\Fields\Traits\ForeignKeyFieldTrait;

/**
 * Class ForeignKeyField
 * @package Lkt\Fields\Runtime
 */
class ReverseForeignKeyField extends AbstractField
{
    use ForeignKeyFieldTrait;

    protected $value = 0;
    protected $initialValue = 0;

    public function setValue($item = null, $value = 0)
    {
        $this->value = (int)$value;
        return $this;
    }

    public function setInitialValue($item = null, $value = 0)
    {
        $this->initialValue = (int)$value;
        return $this->setValue($item, $value);
    }

    /**
     * @param \Lkt\Fields\Runtime\IntegerField $primaryKeyField
     *
     * @return array
     */
    public function getValue(IntegerField $primaryKeyField) :array
    {
        $model = Context::getModel($this->modelIdentifier);
        return ModelFactory::getInstance()
            ->setModel($this->modelIdentifier)
            ->setFrom($model->getTable())
            ->setWhere("{$model->getTable()}.{$this->modelKey} = {$primaryKeyField->getValue()}")
            ->getResults();
    }
}