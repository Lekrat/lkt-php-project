<?php


namespace Lkt\Fields\Runtime;

use Lkt\Fields\Traits\StringFieldTrait;
use Lkt\FieldValidators\FilledStringValidator;
use Lkt\Interfaces\FieldValidatorInterface;

/**
 * Class StringField
 * @package Lkt\Fields\Runtime
 */
class StringField extends AbstractField
{
    use StringFieldTrait;

    protected $value = '';
    protected $initialValue = '';
    protected $isValid = true;
    protected $initialIsValid = true;

    public function setValue($item = null, $value = '')
    {
        $this->value = \trim($value);
        $this->isValid = $this->checkValidator($this->value);
        return $this;
    }

    public function setInitialValue($item = null, $value = '')
    {
        $this->initialValue = \trim($value);
        $this->initialIsValid = $this->checkValidator($this->initialValue);
        return $this->setValue($item, $value);
    }

    /**
     * @param string $value
     *
     * @return bool
     */
    private function checkValidator(string $value = '')
    {
        $validator = $this->validator instanceof FieldValidatorInterface ? $this->validator : FilledStringValidator::getInstance();
        return $validator->handle($value);
    }
}