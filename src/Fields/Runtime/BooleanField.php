<?php


namespace Lkt\Fields\Runtime;

use Lkt\Fields\Traits\BooleanFieldTrait;

/**
 * Class BooleanField
 * @package Lkt\Fields\Runtime
 */
class BooleanField extends AbstractField
{
    use BooleanFieldTrait;

    protected $value = false;
    protected $initialValue = false;

    public function setValue($item = null, $value = false)
    {
        $this->value = $value === true;
        return $this;
    }

    public function setInitialValue($item = null, $value = false)
    {
        $this->initialValue = $value === true;
        return $this->setValue($item, $value);
    }
}