<?php


namespace Lkt\Fields\Runtime;

use Lkt\Fields\Traits\TextFieldTrait;

/**
 * Class TextField
 * @package Lkt\Fields\Runtime
 */
class TextField extends AbstractField
{
    use TextFieldTrait;

    protected $value = '';
    protected $initialValue = '';

    public function setValue($item = null, $value = '')
    {
        $this->value = \trim($value);
        return $this;
    }

    public function setInitialValue($item = null, $value = '')
    {
        $this->initialValue = \trim($value);
        return $this->setValue($item, $value);
    }
}