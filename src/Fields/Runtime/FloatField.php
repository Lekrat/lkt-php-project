<?php


namespace Lkt\Fields\Runtime;

use Lkt\Fields\Traits\FloatFieldTrait;

/**
 * Class FloatField
 * @package Lkt\Fields\Runtime
 */
class FloatField extends AbstractField
{
    use FloatFieldTrait;

    protected $value = 0.0;
    protected $initialValue = 0.0;

    public function setValue($item = null, $value = 0.0)
    {
        $this->value = (float)$value;
        return $this;
    }

    public function setInitialValue($item = null, $value = 0.0)
    {
        $this->initialValue = (float)$value;
        return $this->setValue($item, $value);
    }
}