<?php


namespace Lkt\Fields\Runtime;

use Carbon\Carbon;
use Lkt\Fields\Traits\DateTimeFieldTrait;

/**
 * Class DateTimeField
 * @package Lkt\Fields\Runtime
 */
class DateTimeField extends AbstractField
{
    use DateTimeFieldTrait;

    protected $value = '';
    protected $initialValue = '';
    protected $valueObject;

    public function setValue($item = null, $value = '')
    {
        $this->value = \trim($value);
        $this->valueObject = Carbon::parse($this->value);
        return $this;
    }

    public function setInitialValue($item = null, $value = '')
    {
        $this->initialValue = \trim($value);
        return $this->setValue($item, $value);
    }
}