<?php

namespace Lkt\FieldValidators;

use Lkt\Interfaces\FieldValidatorInterface;
use Lkt\Traits\SingleTon;

class FilledStringValidator implements FieldValidatorInterface
{
    use SingleTon;

    /**
     * @param string $value
     *
     * @return bool
     */
    public function handle(string $value = '') :bool
    {
        return \trim(\strip_tags($value)) !== '';
    }
}