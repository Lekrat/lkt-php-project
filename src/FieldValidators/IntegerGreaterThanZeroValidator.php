<?php

namespace Lkt\FieldValidators;

use Lkt\Interfaces\FieldValidatorInterface;
use Lkt\Traits\SingleTon;

class IntegerGreaterThanZeroValidator implements FieldValidatorInterface
{
    use SingleTon;

    /**
     * @param int $value
     *
     * @return bool
     */
    public function handle(int $value = 0) :bool
    {
        return $value > 0;
    }
}