<?php

namespace Lkt\Multimedia;

use Lkt\Multimedia\Player\Image;
use Lkt\Multimedia\Player\SoundCloud;
use Lkt\Multimedia\Player\Vimeo;
use Lkt\Multimedia\Player\Youtube;

/**
 * Class Source
 *
 * @package Lkt\Multimedia
 */
class Source
{
    const PLAYER_YOUTUBE = 'Youtube';
    const PLAYER_VIMEO = 'Vimeo';
    const PLAYER_SOUND_CLOUD = 'SoundCloud';
    const PLAYER_IMAGE = 'Image';

    const PLAYERS = [
        self::PLAYER_YOUTUBE => Youtube::class,
        self::PLAYER_VIMEO => Vimeo::class,
        self::PLAYER_SOUND_CLOUD => SoundCloud::class,
        self::PLAYER_IMAGE => Image::class,
    ];

    public $src = '';

    public $youtube = '';
    public $vimeo = '';
    public $soundCloud = '';

    public $player = '';

    public $isDetected = false;

    /**
     * Source constructor.
     *
     * @param string $src
     */
    public function __construct($src = '')
    {
        $this->src = \trim($src);

        // Auto Detect
        $this->isDetected = $this->decodeYoutube()
            || $this->decodeVimeo()
            || $this->decodeSoundCloud()
            || $this->decodeImage()
        ;
    }

    /**
     * @return \Lkt\Multimedia\Player\PlayerInterface
     */
    public function getPlayer()
    {
        $className = static::PLAYERS[$this->player];
        return $className::from($this);
    }

    /**
     * @param string $src
     * @return \Lkt\Multimedia\Source
     */
    public static function from($src = '')
    {
        return new static($src);
    }

    /**
     * @return bool
     */
    protected function decodeYoutube()
    {
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $this->src, $match);
        if (\count($match) > 0) {
            $this->youtube = \trim($match[1]);
            $this->player = static::PLAYER_YOUTUBE;
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    protected function decodeVimeo()
    {
        if (preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $this->src, $match)) {
            $this->vimeo = \trim($match[3]);
            $this->player = static::PLAYER_VIMEO;
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    protected function decodeSoundCloud()
    {
        $url = parse_url($this->src);

        if ( count($url) > 2 && !empty($url['host']) && !empty($url['path']) ){
            $host = $url['host'];
            if ( preg_match("/soundcloud\.com/", $host) ){
                $this->soundCloud = $url['path'];
                $this->player = static::PLAYER_SOUND_CLOUD;
                return true;
            }
        }
        return false;
    }

    protected function decodeImage()
    {
        if (preg_match('/(.png|jpg|jpeg|gif|ico|svg)$/', $this->src, $match)) {
            $this->player = static::PLAYER_IMAGE;
            return true;
        }
    }
}