<?php
namespace Lkt\Multimedia\Player;

use Lkt\Helper\HtmlTag;
use Lkt\Multimedia\Source;
use Lkt\Traits\MagicalGet;

/**
 * Class Vimeo
 *
 * @package Lkt\Multimedia\Player
 */
class Vimeo implements PlayerInterface
{
    use MagicalGet;

    protected $source;

    /**
     * Youtube constructor.
     *
     * @param \Lkt\Multimedia\Source $source
     */
    public function __construct(Source $source)
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return "https://player.vimeo.com/video/{$this->source->vimeo}";
    }

    /**
     * @return \Lkt\Helper\HtmlTag
     */
    public function getTag()
    {
        return HtmlTag::getInstance()
            ->setTag('iframe')
            ->setAttribute('data-platform', 'vimeo')
            ->setAttribute('src', $this->getUrl())
            ->setAttribute('width', 640)
            ->setAttribute('height', 306)
            ->setAttribute('frameborder', 0)
            ->setAttribute('webkitallowfullscreen', '')
            ->setAttribute('mozallowfullscreen', '')
            ->setAttribute('allowfullscreen', '');
    }

    /**
     * @param \Lkt\Multimedia\Source $source
     * @return \Lkt\Multimedia\Player\Vimeo
     */
    public static function from(Source $source)
    {
        return new static($source);
    }
}