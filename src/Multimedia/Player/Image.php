<?php

namespace Lkt\Multimedia\Player;

use Lkt\Helper\HtmlTag;
use Lkt\Multimedia\Source;
use Lkt\Traits\MagicalGet;

/**
 * Class Image
 *
 * @package Lkt\Multimedia\Player
 */
class Image implements PlayerInterface
{
    use MagicalGet;

    protected $source;

    /**
     * Youtube constructor.
     *
     * @param \Lkt\Multimedia\Source $source
     */
    public function __construct(Source $source)
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->source->src;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return HtmlTag::getInstance()
            ->setTag('img')
            ->setAttribute('src', $this->getUrl());
    }

    /**
     * @param \Lkt\Multimedia\Source $source
     * @return \Lkt\Multimedia\Player\Youtube
     */
    public static function from(Source $source)
    {
        return new static($source);
    }
}