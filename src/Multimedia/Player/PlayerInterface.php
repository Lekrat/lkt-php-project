<?php

namespace Lkt\Multimedia\Player;

use Lkt\Multimedia\Source;

/**
 * Interface PlayerInterface
 *
 * @package Lkt\Multimedia\Player
 */
interface PlayerInterface
{
    /**
     * @return string
     */
    public function getUrl();

    /**
     * @return \Lkt\Helper\HtmlTag
     */
    public function getTag();

    public static function from(Source $source);
}