<?php

namespace Lkt\Multimedia\Player;

use Lkt\Helper\HtmlTag;
use Lkt\Multimedia\Source;
use Lkt\Traits\MagicalGet;

/**
 * Class SoundCloud
 *
 * @package Lkt\Multimedia\Player
 */
class SoundCloud implements PlayerInterface
{
    use MagicalGet;

    protected $source;

    /**
     * Youtube constructor.
     *
     * @param \Lkt\Multimedia\Source $source
     */
    public function __construct(Source $source)
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return "https://w.soundcloud.com/player/?url={$this->source->src}&amp;auto_play=false&amp;hide_related=true&amp;e&amp;visual=true";
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return HtmlTag::getInstance()
            ->setTag('iframe')
            ->setAttribute('data-platform', 'soundcloud')
            ->setAttribute('src', $this->getUrl())
            ->setAttribute('width', '100%')
            ->setAttribute('height', 166)
            ->setAttribute('frameborder', 'no')
            ->setAttribute('scrolling', 'no');
    }

    /**
     * @param \Lkt\Multimedia\Source $source
     * @return \Lkt\Multimedia\Player\SoundCloud
     */
    public static function from(Source $source)
    {
        return new static($source);
    }
}