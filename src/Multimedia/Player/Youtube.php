<?php

namespace Lkt\Multimedia\Player;

use Lkt\Helper\HtmlTag;
use Lkt\Multimedia\Source;
use Lkt\Traits\MagicalGet;

/**
 * Class Youtube
 *
 * @package Lkt\Multimedia\Player
 */
class Youtube implements PlayerInterface
{
    use MagicalGet;

    protected $source;

    /**
     * Youtube constructor.
     *
     * @param \Lkt\Multimedia\Source $source
     */
    public function __construct(Source $source)
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return "https://www.youtube.com/embed/{$this->source->youtube}";
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return HtmlTag::getInstance()
            ->setTag('iframe')
            ->setAttribute('data-platform', 'youtube')
            ->setAttribute('src', $this->getUrl())
            ->setAttribute('width', 560)
            ->setAttribute('height', 315)
            ->setAttribute('frameborder', 0)
            ->setAttribute('allow', 'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture')
            ->setAttribute('webkitallowfullscreen', '')
            ->setAttribute('mozallowfullscreen', '')
            ->setAttribute('allowfullscreen', '');
    }

    /**
     * @param \Lkt\Multimedia\Source $source
     * @return \Lkt\Multimedia\Player\Youtube
     */
    public static function from(Source $source)
    {
        return new static($source);
    }
}