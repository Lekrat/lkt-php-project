<?php

namespace Lkt\Helper;

use Lkt\Traits\Instantiable;

/**
 * Class Location
 *
 * @package Lkt\Helper
 */
class Location
{
    use Instantiable;

    public $address = '';
    public $postalCode = '';
    public $city = '';
    public $province = '';
    public $country = '';
    public $latitude = '';
    public $longitude = '';
    public $countryId = '';
    public $provinceId = '';

    /**
     * @param string $address
     * @return $this
     */
    public function setAddress($address = ''){
        $this->address = \trim($address);
        return $this;
    }

    /**
     * @param string $postalCode
     * @return $this
     */
    public function setPostalCode($postalCode = ''){
        $this->postalCode = \trim($postalCode);
        return $this;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city = ''){
        $this->city = \trim($city);
        return $this;
    }

    /**
     * @param string $province
     * @return $this
     */
    public function setProvince($province = ''){
        $this->province = \trim($province);
        return $this;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function setCountry($country = ''){
        $this->country = \trim($country);
        return $this;
    }

    /**
     * @param string $countryId
     * @return $this
     */
    public function setCountryId($countryId = ''){
        $this->countryId = \trim($countryId);
        return $this;
    }

    /**
     * @param string $provinceId
     * @return $this
     */
    public function setProvinceId($provinceId = ''){
        $this->provinceId = \trim($provinceId);
        return $this;
    }

    /**
     * @param string $latitude
     * @return $this
     */
    public function setLatitude($latitude = ''){
        $this->latitude = \trim($latitude);
        return $this;
    }

    /**
     * @param string $longitude
     * @return $this
     */
    public function setLongitude($longitude = ''){
        $this->longitude = \trim($longitude);
        return $this;
    }

    /**
     * @return string
     */
    public function toGeocode()
    {
        $r = [];
        if ($this->address !== ''){
            $r[] = \urlencode($this->address);
        }

        if ($this->postalCode !== ''){
            $r[] = \urlencode($this->postalCode);
        }

        if ($this->city !== ''){
            $r[] = \urlencode($this->city);
        }

        if ($this->province !== ''){
            $r[] = \urlencode($this->province);
        }

        if ($this->country !== ''){
            $r[] = \urlencode($this->country);
        }

        return \implode('+', $r);
    }

    /**
     * @return string
     */
    public function toReverseGeocode()
    {
        $r = [];
        if ($this->latitude !== ''){
            $r[] = \urlencode($this->latitude);
        }

        if ($this->longitude !== ''){
            $r[] = \urlencode($this->longitude);
        }

        return \implode(',', $r);
    }

    /**
     * @return string
     */
    public function toString()
    {
        $r = [];
        if ($this->address !== ''){
            $r[] = $this->address;
        }

        if ($this->postalCode !== ''){
            $r[] = $this->postalCode;
        }

        if ($this->city !== ''){
            $r[] = $this->city;
        }

        if ($this->province !== ''){
            $r[] = $this->province;
        }

        if ($this->country !== ''){
            $r[] = $this->country;
        }

        return \implode(', ', $r);
    }

    /**
     * @return string
     */
    public function toStack()
    {
        $r = [];
        if ($this->address !== ''){
            $r[] = $this->address;
        }

        if ($this->postalCode !== ''){
            $r[] = $this->postalCode;
        }

        if ($this->city !== ''){
            $r[] = $this->city;
        }

        if ($this->province !== ''){
            $r[] = $this->province;
        }

        if ($this->country !== ''){
            $r[] = $this->country;
        }

        if ($this->latitude !== ''){
            $r[] = $this->latitude;
        }

        if ($this->longitude !== ''){
            $r[] = $this->longitude;
        }

        return \str_replace(' ', '-', \implode('-', $r));
    }
}