<?php

namespace Lkt\Helper;

use Lkt\Request\Info;

/**
 * Class Link
 *
 * @package Lkt\Helper
 */
class Link
{
    public static function prepare($str = '')
    {
        if ($str === 'http://' || $str === '') {
            return '';
        }
        $url = parse_url($str);
        if (!$url || !isset($url['scheme'])) {
            $requestInfo = Info::getInstance();
            return "{$requestInfo->httpProtocol}://" . $str;
        }
        return $str;
    }
}