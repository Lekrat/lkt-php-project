<?php

namespace Lkt\Helper;


use Lkt\Traits\Instantiable;

class Locations extends \ArrayObject
{
    use Instantiable;
}