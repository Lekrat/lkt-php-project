<?php


namespace Lkt\Helper;

use Lkt\Traits\Instantiable;

/**
 * Class HtmlTag
 *
 * @package Lkt\Helper
 */
class HtmlTag
{
    use Instantiable;

    protected $tag = '';
    protected $id = '';
    protected $name = '';
    protected $classes = [];
    protected $attributes = [];
    protected $content ='';

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id = '')
    {
        $this->id = \trim($id);
        return $this;
    }

    /**
     * @param string $tag
     * @return $this
     */
    public function setTag($tag = '')
    {
        $this->tag = \trim($tag);
        return $this;
    }

    /**
     * @param string $content
     * @return $this
     */
    public function setContent($content = '')
    {
        $this->content = \trim($content);
        return $this;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name = '')
    {
        $this->name = \trim($name);
        return $this;
    }

    /**
     * @param string $className
     * @return $this
     */
    public function addClass($className = '')
    {
        $className = \trim($className);
        $className = explode(' ', $className);
        foreach ($className as $class){
            if (!\in_array($class, $this->classes)){
                $this->classes[] = $class;
            }
        }
        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     * @return $this
     */
    public function setAttribute($name = '', $value = '')
    {
        $this->attributes[\trim($name)] = \trim($value);
        return $this;
    }

    /**
     * @param string $name
     * @return string
     */
    public function getAttribute($name = '')
    {
        return \trim($this->attributes[\trim($name)]);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->parse();
    }

    /**
     * @return string
     */
    public function parse()
    {
        $r = '';
        $body = [];

        if ($this->id !== ''){
            $body[] = "id=\"{$this->id}\"";
        }

        if ($this->name !== ''){
            $body[] = "name=\"{$this->name}\"";
        }

        if (\count($this->classes) > 0){
            $classes = \implode(' ', $this->classes);
            $body[] = "class=\"{$classes}\"";
        }

        foreach ($this->attributes as $attribute => $value){
            if ($value !== ''){
                $body[] = "{$attribute}=\"{$value}\"";
            } else {
                $body[] = "{$attribute}";
            }
        }

        $body = \implode(' ', $body);

        switch ($this->tag){
            case '';
                $r = $this->content;
                break;

            case 'area':
            case 'base':
            case 'br':
            case 'col':
            case 'embed':
            case 'hr':
            case 'img':
            case 'input':
            case 'link':
            case 'meta':
            case 'param':
            case 'source':
            case 'track':
            case 'wbr':
                $r = "<{$this->tag} {$body} />";
                break;

            default:
                $r = "<{$this->tag} {$body}></{$this->tag}>";

        }

        return $r;
    }
}