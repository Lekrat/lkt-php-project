<?php

namespace Lkt\Request;

use Lkt\Traits\SingleTon;

/**
 * Class Info
 *
 * @package Lkt\Request
 */
class Info
{
    use SingleTon;

    public $serverName = '';
    public $currentUrl = '';
    public $currentUrlClean = '';
    public $httpProtocol = '';
    public $protocol = '';
    public $remoteAddress = '';
    public $httpHost = '';
    public $requestUri = '';
    public $requestUriClean = '';

    /**
     * Info constructor.
     */
    public function __construct()
    {
        $this->remoteAddress = \trim($_SERVER['REMOTE_ADDR']);
        $this->httpProtocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') ? 'https' : 'http';

        $protocol = 'HTTP/1.0';
        if ('HTTP/1.1' == $_SERVER['SERVER_PROTOCOL']) {
            $protocol = 'HTTP/1.1';
        }
        $this->protocol = $protocol;
        $this->httpHost = "{$this->httpProtocol}://{$_SERVER['HTTP_HOST']}";

        $this->requestUri = \trim($_SERVER['REQUEST_URI']);
        $this->requestUriClean = \trim(\explode('?', $this->requestUri)[0]);

        $this->currentUrl = "{$this->httpHost}{$this->requestUri}";
        $this->currentUrlClean = "{$this->httpHost}{$this->requestUriClean}";

        $this->serverName = \trim($_SERVER['SERVER_NAME']);
    }
}