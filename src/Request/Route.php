<?php


namespace Lkt\Request;

use Lkt\Traits\Instantiable;
use Lkt\Traits\MagicalGet;

/**
 * Class Route
 *
 * @package Lkt\Request
 */
class Route
{
    use Instantiable;
    use MagicalGet;

    protected $name = '';
    protected $path = '';
    protected $method = '';
    protected $controller = '';

    /**
     * Route constructor.
     *
     * @param string $name
     * @param string $path
     */
    public function __construct(string $method = 'GET', string $name = '', string $path = '')
    {
        $this->method = $method;
        $this->name = $name;
        $this->path = $path;
    }

    public function setController($controller = '')
    {
        $this->controller = $controller;
        return $this;
    }

}