<?php


namespace Lkt\Request;

use Lkt\Traits\Instantiable;
use Lkt\Traits\MagicalGet;

/**
 * Class Route
 *
 * @package Lkt\Request
 */
class RouteGroup
{
    use Instantiable;
    use MagicalGet;

    protected $routes = [];
    protected $name = '';
    protected $route = '';

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name = '') :self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $route
     * @return $this
     */
    public function setRoute($route = '') :self
    {
        $this->route = $route;
        return $this;
    }

    /**
     * @param \Lkt\Request\Route $route
     * @return $this
     */
    public function addRoute(Route $route) :self
    {
        $this->routes[$route->name] = $route;
        return $this;
    }
}