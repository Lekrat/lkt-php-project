<?php

namespace Lkt\Request;

use Lkt\Traits\SingleTon;

/**
 * Class Router
 *
 * @package Lkt\Request
 */
class Router
{
    use SingleTon;

    protected $routes = [];
    protected $groups = [];

    /**
     * @param \Lkt\Request\Route $route
     * @return $this
     */
    public function addRoute(Route $route) :self
    {
        $this->routes[$route->name] = $route;
        return $this;
    }

    /**
     * @param \Lkt\Request\RouteGroup $group
     * @return $this
     */
    public function addGroup(RouteGroup $group) :self
    {
        $this->groups[] = $group;
        return $this;
    }
}