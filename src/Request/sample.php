<?php

use Lkt\Request\Route;
use Lkt\Request\RouteGroup;
use Lkt\Request\Router;

Router::getInstance()
    ->addRoute(Route::getInstance('blog-list', '/blog', 'get'))
    ->addRoute(Route::getInstance('blog-single', '/blog/{identifier}', 'get'))
    ->addGroup(RouteGroup::getInstance()
        ->setName('api')
        ->setRoute('/api')
        ->addRoute(Route::getInstance('create-blog', '/blog/create'))
        ->addRoute(Route::getInstance('edit-blog', '/blog/edit'))
        ->addRoute(Route::getInstance('delete-blog', '/blog/delete'))
    )
    ;