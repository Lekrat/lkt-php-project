<?php

namespace Lkt\Core;

use Lkt\Config\Model;
use Lkt\Drivers\ConnectionInterface;
use Lkt\Traits\SingleTon;

/**
 * Class Context
 *
 * @package Lkt\Core
 */
class Context
{
    use SingleTon;

    protected $connections = [];
    protected $lang = '';
    protected $connectionCacheEnabled = true;
    protected $models = [];

    /**
     * @param string $connectionName
     * @param \Lkt\Drivers\ConnectionInterface $connection
     */
    public static function setConnection($connectionName = 'default', ConnectionInterface $connection)
    {
        static::getInstance()->connections[$connectionName] = $connection;
    }

    /**
     * @param string $connectionName
     * @return ConnectionInterface|null
     */
    public static function getConnection($connectionName = 'default')
    {
        $instance = static::getInstance();
        if (\count($instance->connections) === 0) {
            return null;
        }

        $connection = $instance->connections[$connectionName];
        if (!$connection instanceof ConnectionInterface) {
            return null;
        }

        return $connection;
    }

    public static function getConnections()
    {
        return static::getInstance()->connections;
    }

    /**
     * @param string $lang
     */
    public static function setLang($lang = '')
    {
        static::getInstance()->lang = \trim($lang);
    }

    /**
     * @return string
     */
    public static function getLang()
    {
        return \trim(static::getInstance()->lang);
    }

    /**
     * @param bool $state
     */
    public static function turnConnectionCache($state = true)
    {
        static::getInstance()->connectionCacheEnabled = $state;
    }

    /**
     * @return bool
     */
    public static function hasConnectionCacheEnabled()
    {
        return static::getInstance()->connectionCacheEnabled === true;
    }

    /**
     * @param string $key
     * @param \Lkt\Config\Model $model
     */
    public static function setModel($key = '', Model $model)
    {
        static::getInstance()->models[$key] = $model;
    }

    /**
     * @return \Lkt\Config\Model[]
     */
    public static function getModels() :array
    {
        return static::getInstance()->models;
    }

    /**
     * @param string $modelName
     *
     * @return \Lkt\Config\Model
     */
    public static function getModel($modelName = '') :Model
    {
        return static::getInstance()->models[$modelName];
    }
}