<?php

namespace Lkt\Core;

use Lkt\Config\Model;
use Lkt\Factory\ModelFactory;

/**
 * Class ModelInstance
 * @package Lkt\Core
 */
class ModelInstance
{
    protected static $MODEL;
    public $fields;

    /**
     * ModelInstance constructor.
     *
     * @param \Lkt\Config\Model $model
     * @param array $data
     */
    public function __construct(Model $model, array $data = [])
    {
        if (!static::$MODEL){
            static::$MODEL = $model->getIdentifier();
        }
        $this->fields = new \StdClass();
        foreach ($model->getFields() as $key => $value){
            $field = $model->getField($key);
            $runtimeField = $field->getRuntimeField($this, $data[$key]);
            $this->fields->{$key} = $runtimeField
                ->setValidator($field->getValidator())
                ->setColumnName($field->getColumnName())
                ->setApplicationName($field->getApplicationName())
                ->setDefaultValue($field->getDefaultValue())
                ->setPrimaryKey($field->isPrimaryKey())
            ;
        }
    }

    /**
     * @return \Lkt\Fields\Runtime\AbstractField
     */
    public function getPrimaryKeyField()
    {
        foreach ($this->fields as $field){
            if ($field->isPrimaryKey()){
                return $field;
            }
        }

        return $this->fields[0];
    }

    /**
     * @return \Lkt\Config\Model
     */
    public function getModel()
    {
        return Context::getModel(static::$MODEL);
    }

    /**
     * @return \Lkt\Factory\ModelFactory
     */
    public static function factory() :ModelFactory
    {
        return ModelFactory::getInstance()->setModel(static::$MODEL);
    }
}