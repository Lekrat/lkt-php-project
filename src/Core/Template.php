<?php

namespace Lkt\Core;

use Lkt\Traits\Instantiable;

/**
 * Class Template
 * @package Lkt\Core
 */
class Template
{
    use Instantiable;

    /** @var  \League\Plates\Engine */
    protected $template;
    protected $data = [];
    protected $file = '';

    /**
     * Template constructor.
     *
     * @param string $path
     * @param string $file
     * @param string $extension
     */
    public function __construct($path = '', $file = '', $extension = 'tpl')
    {
        $this->template = new \League\Plates\Engine($path, $extension);
        $this->file = $file;
    }

    /**
     * @param string $key
     * @param string $value
     *
     * @return $this
     */
    public function set(string $key = '', $value = '')
    {
        $this->data[$key] = $value;
        return $this;
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        return $this->template->render($this->file, $this->data);
    }
}