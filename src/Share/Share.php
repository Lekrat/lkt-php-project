<?php

namespace Lkt\Share;

/**
 * Class Share
 *
 * @package Lkt\Share
 */
class Share
{
    /**
     * @param string $to
     * @param string $subject
     * @param string $msg
     * @return string
     */
    public static function mailTo($to = 'example@example.com', $subject = '', $msg = '')
    {
        return "mailto:{$to}?subject={$subject}&body={$msg}";
    }

    /**
     * @param string $url
     * @return string
     */
    public static function facebook($url = '')
    {
        return "https://www.facebook.com/sharer/sharer.php?u={$url}";
    }

    /**
     * @param string $url
     * @return string
     */
    public static function googlePlus($url = '')
    {
        return "https://plus.google.com/share?url={$url}";
    }

    /**
     * @param string $url
     * @param string $title
     * @param string $summary
     * @param string $source
     * @return string
     */
    public static function linkedIn($url = '', $title = '', $summary = '', $source = '')
    {
        $url = htmlentities($url, ENT_QUOTES, 'UTF-8');
        $title = htmlentities($title, ENT_QUOTES, 'UTF-8');
        $summary = htmlentities($summary, ENT_QUOTES, 'UTF-8');
        $source = htmlentities($source, ENT_QUOTES, 'UTF-8');
        return "https://www.linkedin.com/shareArticle?mini=true&url={$url}&title={$title}&summary={$summary}&source={$source}";
    }

    /**
     * @param string $url
     * @param string $media
     * @param string $title
     * @return string
     */
    public static function pinterest($url = '', $media = '', $title = '')
    {
        return "//es.pinterest.com/pin/create/button/?url={$url}&media={$media}&description={$title}";
    }

    /**
     * @param string $url
     * @return string
     */
    public static function rss($url = '')
    {
        return $url;
    }

    /**
     * @param string $url
     * @param string $via
     * @return string
     */
    public static function twitter($url = '', $via = '')
    {
        $via = urlencode($via);
        return "https://twitter.com/intent/tweet?via={$via}&url={$url}";
    }

    /**
     * @param string $url
     * @param string $text
     * @return string
     */
    public static function whatsApp($url = '', $text = '')
    {
        $url = urlencode($url);
        $text = htmlentities($text, ENT_QUOTES, 'UTF-8');
        return "whatsapp://send?text={$text} {$url}";
    }

    /******************************************
     * onGoogleCalendar
     *
     * @param array $args
     * * $args['begin'] => Start Date (YYYYMMAATHHMMSS) Required
     * * $args['end'] => End date (YYYYMMAATHHMMSS) Required
     * * $args['location'] => Location Required
     * * $args['details'] => Details Required
     * * $args['title'] => Title Required
     * * Sample:
     *     "https://calendar.google.com/calendar/render?action=TEMPLATE&dates=20160504T110000Z/20160504T170000Z&location=Tatooine&text=Star+Wars+Day+Party&details=May+the+force+be+with+you";
     ******************************************@return string
     * @return string
     */
    public static function onGoogleCalendar($args = [])
    {

        $url = 'https://calendar.google.com/calendar/render?action=TEMPLATE';

        if (strlen($args['begin']) > 0 && strlen($args['end']) > 0) {
            $url .= "&dates={$args['begin']}/{$args['end']}";

        } elseif (strlen($args['begin']) > 0) {
            $url .= "&dates={$args['begin']}";
        }

        if (strlen($args['location']) > 0) {
            $url .= '&location=' . urlencode($args['location']);
        }

        if (strlen($args['title']) > 0) {
            $url .= '&text=' . urlencode($args['title']);
        }

        if (strlen($args['details']) > 0) {
            $url .= '&details=' . urlencode($args['details']);
        }

        return $url;
    }


    /******************************************
     * onGoogleCalendar
     *
     * @param array $args
     * * $args['begin'] => Start Date (YYYYMMAATHHMMSS) Required
     * * $args['end'] => End date (YYYYMMAATHHMMSS) Required
     * * $args['location'] => Location Required
     * * $args['details'] => Details Required
     * * $args['title'] => Title Required
     * * Sample:
     *     "https://calendar.google.com/calendar/render?action=TEMPLATE&dates=20160504T110000Z/20160504T170000Z&location=Tatooine&text=Star+Wars+Day+Party&details=May+the+force+be+with+you";
     ******************************************@return string
     * @return string
     */
    public static function onOutlook($args = [])
    {

        $url = 'https://bay02.calendar.live.com/calendar/calendar.aspx?rru=addevent';

        if (strlen($args['begin']) > 0 && strlen($args['end']) > 0) {
            $url .= "&dates={$args['begin']}/{$args['end']}";

        } elseif (strlen($args['begin']) > 0) {
            $url .= "&dates={$args['begin']}";
        }

        if (strlen($args['location']) > 0) {
            $url .= '&location=' . urlencode($args['location']);
        }

        if (strlen($args['title']) > 0) {
            $url .= '&summary=' . urlencode($args['title']);
        }

        if (strlen($args['details']) > 0) {
            $url .= '&description=' . urlencode($args['details']);
        }

        return $url;
    }
}