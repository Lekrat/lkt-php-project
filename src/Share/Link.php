<?php

namespace Lkt\Share;

/**
 * Class Link
 *
 * @package Lkt\Share
 */
class Link
{
    protected $url = '';
    protected $subject = '';
    protected $message = '';
    protected $image = '';

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url = '')
    {
        $this->url = \trim($url);
        return $this;
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject($subject = '')
    {
        $this->subject = \trim($subject);
        return $this;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage($message = '')
    {
        $this->message = \trim($message);
        return $this;
    }

    /**
     * @param string $image
     * @return $this
     */
    public function setImage($image = '')
    {
        $this->image = \trim($image);
        return $this;
    }

    public function mail()
    {
        $args = [];
        if ($this->subject !== ''){
            $args[] = $this->subject;
        }
        if ($this->message !== ''){
            $args[] = $this->message;
        }
        $args = \trim(\implode('&', $args));
        if ($args !== ''){
            $args = "?{$args}";
        }

        return "mailto:{$this->url}{$args}";
    }

    public function phone()
    {
        return "tel:{$this->url}";
    }
}