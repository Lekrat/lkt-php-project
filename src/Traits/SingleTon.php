<?php

namespace Lkt\Traits;


trait SingleTon
{
    /**
     * @var static
     */
    protected static $Instance;

    /**
     * @param bool $forceFresh
     * @return static
     */
    public static function getInstance($forceFresh = false)
    {
        if ($forceFresh === true || !\is_object(self::$Instance)){
            self::$Instance = new static();
            if (\method_exists(self::$Instance, 'PostAwake')){
                self::$Instance->PostAwake();
            }
        }

        return self::$Instance;
    }
}