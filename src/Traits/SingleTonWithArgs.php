<?php

namespace Lkt\Traits;


trait SingleTonWithArgs
{
    /**
     * @var static
     */
    protected static $Instance;

    /**
     * @param bool $forceFresh
     * @return static
     */
    public static function getInstance(...$args)
    {
        if (!\is_object(self::$Instance)){
            self::$Instance = new static(...$args);
            if (\method_exists(self::$Instance, 'PostAwake')){
                self::$Instance->PostAwake();
            }
        }

        return self::$Instance;
    }
}