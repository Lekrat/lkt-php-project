<?php

namespace Lkt\Traits\Config;

/**
 * Trait WithModelIdentifierTrait
 * @package Lkt\Traits\Config
 */
trait WithModelIdentifierTrait
{
    protected $modelIdentifier = '';

    /**
     * @param $identifier
     * @return $this
     */
    public function setModelIdentifier($identifier) :self
    {
        $this->modelIdentifier = $identifier;
        return $this;
    }
}