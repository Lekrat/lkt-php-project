<?php

namespace Lkt\Traits;

/**
 * Trait Instantiable
 *
 * @package Lkt\Traits
 */
trait Instantiable
{
    /**
     * @return static
     */
    public static function getInstance(...$args)
    {
        $r = new static(...$args);

        if (\method_exists($r, 'PostAwake')){
            $r->PostAwake();
        }

        return $r;
    }
}