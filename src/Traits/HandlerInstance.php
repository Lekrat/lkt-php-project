<?php


namespace Lkt\Traits;


trait HandlerInstance
{
    /**
     * @return static
     */
    public static function getInstance(...$args)
    {
        $r = new static(...$args);

        if (\method_exists($r, 'PostAwake')){
            return $r->PostAwake();
        }

        return null;
    }
}