<?php

namespace Lkt\Traits;

/**
 * Trait MagicalGet
 *
 * @package Lkt\Traits
 */
trait MagicalGet
{
    /**
     * @param $name
     * @return string
     */
    public function __get($name)
    {
        if (isset($this->{$name})){
            return $this->{$name};
        }
        return '';
    }
}