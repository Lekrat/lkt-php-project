<?php

namespace Lkt\Factory;

use Lkt\Core\Context;
use Lkt\Traits\Instantiable;

/**
 * Class ModelFactory
 * @package Lkt\Factory
 */
class ModelFactory
{
    use Instantiable;

    protected $modelName = '';
    /** @var \Lkt\Config\Model */
    protected $model;
    protected $connectionName = 'default';

    protected $select = '';
    protected $selectAppend = '';

    protected $from = '';
    protected $fromAppend = '';

    protected $where = '1';
    protected $whereAppend = '';

    protected $orderBy = '';

    protected $page = -1;
    protected $limit = -1;

    protected $skipCache = false;
    protected $asArray = false;

    protected $modId = 0;
    protected $sectionId = 0;
    protected $referencedId = 0;
    protected $referencedType = 0;

    protected $isSingleMode = false;
    protected $singleWithoutPrimaryKey = false;

    protected $rawQuery = '';

    protected static $CACHE = [];

    /**
     * @param string $modelName
     *
     * @return $this
     */
    public function setModel($modelName = '') :self
    {
        $this->modelName = $modelName;
        $this->model = Context::getModel($this->modelName);
        return $this;
    }

    public function setConnectionName($connectionName = 'default') :self
    {
        $this->connectionName = $connectionName;
        return $this;
    }

    /**
     * @return array|bool
     */
    protected function getRawResults()
    {
        $db = Context::getConnection($this->connectionName);
        return $db->query($this->getQuery());
    }

    /**
     * @return string
     */
    public function getQuery() :string
    {
        if ($this->rawQuery !== ''){
            return $this->rawQuery;
        }

        $select = $this->select;
        $from = $this->from;
        $where = $this->where;

        if ($select === ''){
            $select = $this->model->getQueryFields();
        }
        if ($from === '') {
            $from = $this->model->getTable();
        }

        if ($this->fromAppend !== '') {
            $from .= $this->fromAppend;
        }

        if ($this->whereAppend !== '') {
            $where .= ' ' .$this->whereAppend;
        }

        $query = "SELECT {$select} FROM {$from} WHERE {$where}";

        if ($this->orderBy !== '') {
            $query .= "
            ORDER BY {$this->orderBy}";
        }

        if ($this->limit > -1){
            if ($this->page > -1){
                $p = $this->page * $this->limit;
                $query .= "
            LIMIT {$p}, {$this->limit}";

            } else {
                $query .= "
            LIMIT {$this->limit}";
            }
        }

        return $query;
    }

    /**
     * @return array
     */
    public function getResults() :array
    {
        $results = $this->getRawResults();
        if (\count($results) === 0){
            return [];
        }

        $items = [];
        foreach ($results as $iAux) {
            $primaryKeyValue = $iAux[$this->model->getPrimaryKeyField()];
            if ($this->inCache($this->modelName, $primaryKeyValue)){
                $items[] = $this->loadCache($this->modelName, $primaryKeyValue);
            } else {
                $className = $this->model->getClassName();
                $instance = new $className($this->model, $iAux);
                $this->storeCache($this->modelName, $primaryKeyValue, $instance);
                $items[] = $this->loadCache($this->modelName, $primaryKeyValue);
            }
        }

        return $items;
    }

    /**
     * @param string $countableField
     *
     * @return int
     */
    public function getCount($countableField = '') :int
    {
        $this->select = "COUNT($countableField) as Count";
        $r = $this->getRawResults();
        return (int)$r[0]['Count'];
    }

    /**
     * @param string $countableField
     * @return string
     */
    public function getCountQuery($countableField = '') :string
    {
        $this->select = "COUNT($countableField) as Count";
        return $this->getQuery();
    }

    /**
     * @param int $moduleType
     *
     * @return $this
     */
    public function setModule($moduleType = 0) :self
    {
        $this->moduleType = $moduleType;
        return $this;
    }

    /**
     * @param string $model
     *
     * @return $this
     */
    public function setSqlModel($model = '') :self
    {
        $this->sqlModel = $model;
        return $this;
    }

    /**
     * @param string $q
     *
     * @return $this
     */
    public function setSelect($q = '') :self
    {
        $this->select = $q;
        return $this;
    }

    /**
     * @param string $q
     *
     * @return $this
     */
    public function setFrom($q = '') :self
    {
        $this->from = $q;
        return $this;
    }

    /**
     * @param string $q
     *
     * @return $this
     */
    public function setFromAppend($q = '') :self
    {
        $this->fromAppend = $q;
        return $this;
    }

    /**
     * @param string $q
     *
     * @return $this
     */
    public function setWhere($q = '') :self
    {
        $this->where = $q;
        return $this;
    }

    /**
     * @param string $q
     *
     * @return $this
     */
    public function setWhereAppend($q = '') :self
    {
        $this->whereAppend = $q;
        return $this;
    }

    /**
     * @param string $q
     *
     * @return $this
     */
    public function setOrderBy($q = '') :self
    {
        $this->orderBy = $q;
        return $this;
    }

    /**
     * @param int $page
     *
     * @return $this
     */
    public function setPage($page = 1) :self
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @param int $limit
     *
     * @return $this
     */
    public function setLimit($limit = 1) :self
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @param bool $bool
     *
     * @return $this
     */
    public function setSingleWithoutPrimaryKey($bool = true) :self
    {
        $this->singleWithoutPrimaryKey = $bool;
        return $this;
    }

    /**
     * @param string $sql
     *
     * @return $this
     */
    public function setRaw($sql = '') :self
    {
        $this->rawQuery = $sql;
        return $this;
    }

    /**
     * @param string $modelName
     * @param int $itemId
     *
     * @return bool
     */
    protected function inCache($modelName = '', $itemId = 0) :bool
    {
        return \is_object(self::$CACHE[$modelName][$itemId]);
    }

    /**
     * @param string $modelName
     * @param int $itemId
     *
     * @return mixed
     */
    protected function loadCache($modelName = '', $itemId = 0)
    {
        return self::$CACHE[$modelName][$itemId];
    }

    /**
     * @param string $modelName
     * @param int $itemId
     * @param null $item
     */
    protected function storeCache($modelName = '', $itemId = 0, $item = null)
    {
        self::$CACHE[$modelName][$itemId] = $item;
    }
}