namespace <?php echo $this->e($namespace); ?>;

class <?php echo $this->e($className); ?>  extends \Lkt\Core\ModelInstance
{

    <?php
        echo $this->data['getters'];
        echo $this->data['setters'];
    ?>
}