<?php

use Lkt\Config\Model;
use Lkt\Fields\Config\BooleanField;
use Lkt\Fields\Config\DateTimeField;
use Lkt\Fields\Config\FloatField;
use Lkt\Fields\Config\ForeignKeyField;
use Lkt\Fields\Config\IntegerField;
use Lkt\Fields\Config\PivotField;
use Lkt\Fields\Config\StringField;

Model::getInstance()
    ->setIdentifier('sites-periods')
    ->setTable('iw4_sites_periods')
    ->setFields([
        IntegerField::getInstance()
            ->setColumnName('iw4id')
            ->setApplicationName('id'),

        DateTimeField::getInstance()
            ->setColumnName('start_date')
            ->setApplicationName('startDate'),

        DateTimeField::getInstance()
            ->setColumnName('end_date')
            ->setApplicationName('endDate'),

        FloatField::getInstance()
            ->setColumnName('budget')
            ->setApplicationName('budget'),

        ForeignKeyField::getInstance()
            ->setColumnName('1n_sites')
            ->setApplicationName('siteId')
            ->setModelIdentifier('sites')
            ->setModelKey('iw4id'),

        BooleanField::getInstance()
            ->setColumnName('is_initialized')
            ->setApplicationName('isInitialized'),

        StringField::getInstance()
            ->setColumnName('desired_savings')
            ->setApplicationName('desiredSavings'),

        StringField::getInstance()
            ->setColumnName('projected_changes')
            ->setApplicationName('projectedChanges'),

        PivotField::getInstance()
            ->setApplicationName('opportunities')
            ->setModelIdentifier('opportunities'),

        PivotField::getInstance()
            ->setApplicationName('openedOpportunities')
            ->setModelIdentifier('opportunities')
            ->setColumnName('id1')
            ->setModelKey('id2')
            ->setPivotTable('iw4_sites_periods_nn_opportunities')
            ->addConstraint("status = 'started'"),

        PivotField::getInstance()
            ->setApplicationName('budgetAmount')
            ->setColumnName('1n_budget_amount')
            ->setModelKey('iw4id')
            ->setModeMultipleToOne(),
    ])
    ->save();